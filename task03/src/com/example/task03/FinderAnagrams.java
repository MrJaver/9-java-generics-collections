package com.example.task03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FinderAnagrams {
    private ArrayList<String> list = new ArrayList<>();

    private boolean isRussianWord(String string) {
        Pattern pattern = Pattern.compile("[а-яА-ЯёЁ]*");
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    private boolean isLengthMoreThree(String string) {
        return string.length() > 3;
    }

    private HashMap<Character, Integer> makeMapFrequencyOfCharacter(String string) {
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            map.merge(c, 1, Integer::sum);
        }
        return map;
    }

    private HashMap<HashMap<Character, Integer>, HashSet<String>> makeProtoList() {
        HashMap<HashMap<Character, Integer>, HashSet<String>> map = new HashMap<>();
        for (String string : list) {
            HashMap<Character, Integer> hash = makeMapFrequencyOfCharacter(string);
            HashSet<String> set = map.get(hash);
            if(set!=null)
            {
                set.add(string);
                map.put(hash,set);
            }
            else {
                HashSet<String> set1 =new HashSet<>();
                set1.add(string);
                map.put(hash,set1);
            }
        }
        return map;
    }

    public List<Set<String>> makeList(){
        HashMap<HashMap<Character, Integer>, HashSet<String>> map = makeProtoList();

        List<Set<String>> list = new ArrayList<>();
        for(Map.Entry<HashMap<Character, Integer>, HashSet<String>> set: map.entrySet()){
            if(set.getValue().size()!=1) {
                list.add(new TreeSet<>(set.getValue()));
            }
        }
        Collections.sort(list,new LexicographicComparator());
        return list;
    }

    public FinderAnagrams(InputStream inputStream, Charset charset) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, charset));) {
            String line;
            while ((line = bufferedReader.readLine()) != null)
                if (isRussianWord(line) && isLengthMoreThree(line))
                    list.add(line.toLowerCase());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
