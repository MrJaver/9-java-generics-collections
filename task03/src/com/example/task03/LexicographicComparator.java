package com.example.task03;

import java.util.Comparator;
import java.util.Set;

public class LexicographicComparator implements Comparator<Set<String>> {
    @Override
    public int compare(Set<String> o1, Set<String> o2) {
        return o1.iterator().next().compareTo(o2.iterator().next()) ;
    }
}
