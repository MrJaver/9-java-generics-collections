package com.example.task03;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Set;

public class Task03Main {

    public static void main(String[] args) throws IOException {

        List<Set<String>> anagrams = findAnagrams(new FileInputStream("task03/resources/singular.txt"), Charset.forName("windows-1251"));
        for (Set<String> anagram : anagrams) {
            System.out.println(anagram);
        }

    }

    public static List<Set<String>> findAnagrams(InputStream inputStream, Charset charset) {
        FinderAnagrams finderAnagrams = new FinderAnagrams(inputStream,charset);
        return finderAnagrams.makeList();

    }
}
