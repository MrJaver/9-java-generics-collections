package com.example.task01;

import java.util.function.BiConsumer;
public class Pair<T, D> {
    private T first = null;
    private D second = null;

    private Pair() {
    }

    private Pair(T first, D second) {
        this.first = first;
        this.second = second;
    }

    public static <T, D> Pair of(T first, D second) {
        return new PairBuilder<T,D>().setFirst(first).setSecond(second).build();
    }

    public T getFirst() {
        return this.first;
    }

    public D getSecond() {
        return this.second;
    }

    public int hashCode() {
        String hashStr =first.toString()+second.toString();
        return hashStr.hashCode();
    }

    public boolean equals(Pair pair) {
        return this.first.equals(pair.first) && this.second.equals(pair.second);
    }

    public <T,D> void ifPresent(BiConsumer biConsumer){
        if(this.first!=null && this.second!=null){
            biConsumer.accept(this.first,this.second);
        }
    }

    public static class PairBuilder<T, D> {
        private T first = null;
        private D second = null;

        public PairBuilder<T, D> setFirst(T first) {
            this.first = first;
            return this;
        }

        public PairBuilder<T, D> setSecond(D second) {
            this.second = second;
            return this;
        }

        public Pair<T, D> build() {
            return new Pair<>(this.first, this.second);
        }
    }

}
