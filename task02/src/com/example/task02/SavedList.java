package com.example.task02;

import java.io.*;
import java.nio.file.Files;
import java.util.AbstractList;
import java.util.ArrayList;

public class SavedList<E extends Serializable> extends AbstractList<E> implements Serializable {

    private AbstractList<E> list = new ArrayList<>();
    private final File file;

    private void saveInFile() {
        try (
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
        ) {
            oos.writeObject(list);
            oos.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private void loadFromFile() {
        list.clear();
        try (
                FileInputStream f= new FileInputStream(file);
                ObjectInputStream objectInputStream = new ObjectInputStream(f);
        ) {
            list = (ArrayList<E>)objectInputStream.readObject();
            objectInputStream.close();
            f.close();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            return;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public SavedList(File file) {
        super();
        this.file = file;
        try {
            file.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        loadFromFile();
    }

    @Override
    public E get(int index) {
        return list.get(index);
    }

    @Override
    public E set(int index, E element) {
        E e = list.set(index, element);
        saveInFile();
        return e;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public void add(int index, E element) {
        list.add(index, element);
        saveInFile();
    }

    @Override
    public E remove(int index) {
        E e = list.remove(index);
        saveInFile();
        return e;
    }
}
